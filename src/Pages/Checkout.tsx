import CheckoutTemplate from "components/templates/CheckoutTemplate";

const Checkout = () => {
  return (
    <section className="pb-24 bg-slate-50 pt-12">
      <CheckoutTemplate />
    </section>
  );
};

export default Checkout;
